+++
fragment = "content"
date = "2023-01-30"
weight = 10
title = "Profitorientierung und FOSS -- ein Gegensatz?"
background = "white"

summary = "velocitux arbeitet ausschließlich mit und an FOSS, unter anderem am freien Schulinformationssystem AlekSIS. Aber wie ist eigentlich das Verhältnis zwischen FOSS und Profitorientierung?"

display_date = true

[asset]
image = ""
+++

Entwicklung von freier Open-Source-Software – das klingt intuitiv nach einer
Menge freiwilliger Arbeit. Genauso sah und sieht es bei dem
Schulinformationssystem [AlekSIS](https://aleksis.org) aus. Geboren aus der
Fusion zweier sehr ähnlicher Vorgängerprojekte, entwickelte sich AlekSIS durch
die ehrenamtliche Arbeit einer kleinen, engagierten Entwicklergruppe in den
letzten Jahren zu einer umfangreichen und erweiterbaren Softwarelösung für
vielfältige Anwendungsbereiche in nicht nur schulischen Kontexten. Mittlerweile
wird es an mehreren Schulen im Produktivbetrieb eingesetzt – und wir von
Velocitux bieten professionellen Betrieb, Support & Anpassungen am Code für
AlekSIS an.

Aber Moment: Heißt das nicht, dass wir gegen Bezahlung an FOSS entwickeln? Ja.
Widerspricht das den Idealen dieser Bewegung nicht ein bisschen? Nein, zumindest
nicht immer.

Velocitux bietet ausschließlich Dienstleistungen im Kontext von freier
Open-Source-Software an. Das machen wir, weil wir, die Menschen rund um
Velocitux im Laufe der Jahre vielfältige Erfahrungen in der Arbeit mit FOSS
gesammelt haben und gerade deswegen davon überzeugt sind, dass die dazugehörigen
Ideale zu einer besseren Welt beitragen können – denn sie schafft Transparenz
und Unabhängigkeit für Nutzer\*innen, sichert Teilhabe für Interessierte, betont
einen Anspruch auf Gleichheit im finanziellen Sinne und schafft Vertrauen in
Anwendungen, auf die wir alle zunehmend angewiesen sind. Dabei ist uns der
Einsatz von FOSS rund um den Bereich Bildung besonders wichtig. Schließlich
stellt die Verwendung von Freier Software schon an und für sich ein Lernprozess
für alle Beteiligten dar – und bietet zudem vielfältige
Partizipationsmöglichkeiten für die Nutzer\*innen. Nicht zuletzt haben viele von
uns bereits viele Erfahrungen an der Schnittstelle zwischen Bildung und Software
gemacht, u. a. durch unsere Beteiligung am AlekSIS-Projekt. Daher ist es uns ein
Herzensanliegen, mit zusätzlichen Mitteln dieses Projekt zu stärken – auch
solchen, die wir als ehrenamtliches Entwicklerteam nicht bereitstellen könnten.
Die Möglichkeit, bestimmte Features gegen Entgelt implementieren zu lassen und
professionelle Unterstützung im gesamten Prozess von der Idee, ein
Schulinformationssystem einzusetzen, bis zur tatsächlichen Inbetriebnahme in
Anspruch nehmen zu können, rundet für Interessierte das Gesamtpaket ab und
ermöglicht erst eine großflächige Verbreitung der Software.

Hallo, mein Name ist Hangzhi und ich bin seit neuestem Werkstudent bei
Velocitux. Zu meiner Arbeit gehört vor allem, auf Wunsch bestimmter
Institutionen – das können zum Beispiel Schulen oder kommunale Schulträger sein
– Anpassung in Form von gewünschten Features in AlekSIS zu konzipieren und zu
implementieren. Im Grunde also von der reinen Arbeit her nichts
Grundverschiedenes als das, was ich in meiner ehrenamtlichen Arbeit für das
Projekt seit Jahren tue. Nur: Die grobe Richtung meiner Arbeit ist Ergebnis
einer Absprache mit externen Akteuren und nicht einer internen Priorisierung.

Vieles hat sich mit der Möglichkeit des bezahlten Arbeitens an AlekSIS
verändert. Zunächst ein wenig überraschend, aber mir am wichtigsten ist das Mehr
an Freiheit, das mit bezahlter Arbeit einherging. Oder genauer gesagt: Die
Freiheiten, die ich am ehrenamtlichen Arbeiten sehr schätze – “Arbeiten nach
Interesse”, relativ freie Zeiteinteilung, wenig Abhängigkeit von Externen etc. –
werden durch andere ergänzt. Mittlerweile brauche ich mir zum Beispiel kaum
Stress zu machen, wenn ich versuche, möglichst regelmäßige AlekSIS-Arbeit,
anderes Engagement, mein Studium und alles andere Unbezahlte im Leben unter
einen Hut zu bringen und dabei immer den nervigen Gedanken im Hinterkopf habe,
dass man zwischen all diesen Dingen noch irgendwie eine Erwerbstätigkeit
unterbringen müsste. Das Abwägen zwischen eigenen Interessen und Arbeit aus rein
finanziellen Gründen raubt in vielen Fällen die Freiheit, die eigene Zeit
selbstbestimmt und sinnstiftend einzuteilen.

Klar: Bezahlt werden bedeutet auch, ein gewisses Maß an Verbindlichkeit
hinzunehmen. Deadlines und strukturierte Aufgabenplanung waren nie meine besten
Freunde, und anfangs hatte ich bisweilen meine Schwierigkeiten damit, im Voraus
meine zu erledigende Arbeit einzuteilen. Gleichzeitig bin ich im Nachhinein froh
darüber, dass diese Zunahme an Verbindlichkeit und Struktur ein bisschen auf das
gesamte AlekSIS-Projekt abgefärbt hat – und dafür sorgt, dass wir in unserer
freiwilligen Arbeit stressfreier und trotzdem schneller vorankommen und besser
in der Lage sind, die zukünftige Entwicklung des Projektes zu steuern. Ich
empfinde es auch immer wieder als herausfordernd und zugleich bereichernd, mich
im Rahmen von externen Aufträgen mit Teilbereichen der Webentwicklung zu
beschäftigen und schließlich Dinge zu lernen, mit denen ich mich sonst nie
auseinandergesetzt hätte. Man könnte wohl sagen, dass das bezahlte Arbeiten am
Projekt meinen technischen Horizont zwangsläufig erweitert hat.

Um aber auf die anfangs aufgeworfene Frage zurückzukommen: FOSS und
Profitorientierung sind kein natürliches Begriffspaar. Sie schließen sich aber
auch nicht aus, denn Freiheit impliziert in der üblichen FOSS-Definition keine
Kostenfreiheit. Die Entwicklung und Anwendung von FOSS findet nicht in einer
profitlosen Parallelwelt statt, sondern ganz wesentlich im aktuellen,
mehrheitlich gewinnorientierten Wirtschaftssystem – und das ist auch vollkommen
okay so, solange die Idee der Profitorientierung an sich nicht infrage gestellt
wird. Ich bin davon überzeugt, dass das Maß an Verbreitung, das FOSS in diversen
Bereichen mittlerweile erreicht hat, ohne die Arbeit von gewinnorientierten
Unternehmen und Einzelpersonen nie möglich gewesen wäre. Denkt man zum Beispiel
an per definitionem viel externen Support benötigende Bereiche der IT, zum
Beispiel Webhosting-Dienstleistungen, dann ist es schwer vorstellbar, wie ein
großflächiger Einsatz ohne ein solides Gerüst an externen
Unterstützungsdienstleistern möglich sein soll. Auch ergibt es in vielen Fällen
für (mitunter gewinnorientierte) Institutionen selber Sinn, teils auf FOSS zu
setzen, zum Beispiel, um einen Lock-in auf proprietäre Software zu verhindern –
was ein Teilaspekt der Freiheit im Sinne der Freie-Software-Bewegung ist. In
diesem Kontext trägt der Einsatz von FOSS u. a. auch zu einer Reduktion an
redundanter Arbeit und stellen somit eine Chance für effizientes Wirtschaften
dar. Oft ist aber festzustellen, dass profitorientierte Unternehmen in Bezug auf
FOSS ganz pragmatisch Partikularinteressen und keinen weit gedachten Idealismus
verfolgen. So werden zwar wesentliche Teile von vielen großen FOSS-Projekten
durch Mitarbeitende solcher Unternehmen entwickelt, die damit konkrete Ziele bei
möglichst hoher Kosteneffizienz verfolgen wollen. Das ist wieder an sich nicht
verwerflich – schließlich tragen sie damit große Teile zu frei nutzbaren
FOSS-Projekten bei – wirft jedoch die Frage nach der Notwendigkeit von solchen
Interessen unabhängiger Kontrollinstitutionen oder von Profitorientierung
losgelöster Finanzierungswege auf. Wenn man FOSS, zumindest in einzelnen
Bereichen, als Gemeingut im weiteren Sinne und nicht nur als wirtschaftliches
Kalkül betrachtet, dann ergibt sich eine Unabdingbarkeit einer Entkopplung
tiefgreifender Gestaltungsmöglichkeiten bei FOSS von der Macht
profitorientierter Unternehmen. Die Ideale von FOSS stellen einen
gesellschaftspolitischen Auftrag dar. Um diesen zu erfüllen, müssen Strukturen,
die eine effiziente, gesteuerte Weiterentwicklung von FOSS unter
gemeinwohlorientierten Gesichtspunkten erlauben und gleichzeitig reale
wirtschaftlicher Gegebenheiten im Blick behalten, geschaffen werden – zum
Beispiel eine öffentliche Finanzierung entsprechender Projekte in besonders
essenziellen Sektoren oder durch die Etablierung und Förderung von unabhängigen
Trägerstrukturen mit gewissen finanziellen Mitteln für FOSS-Projekte in der
Zivilgesellschaft. Der FOSS-eigene Anspruch an Freiheit und Offenheit muss sich
auch auf den Entwicklungsprozess übertrage: Menschen, die ein Großteil ihrer
Zeit mit der Arbeit an solchen Projekten verbringen, sollten in bestimmten
Fällen eine finanzielle Zuwendung entkoppelt von unternehmensspezifischen
Interessen erhalten können. Nur so kann die Unabhängigkeit von FOSS gesichert
werden und Stabilität in ein ansonsten durch wechselnde Partikularinteressen
aufgewirbeltes Feld gebracht werden.
