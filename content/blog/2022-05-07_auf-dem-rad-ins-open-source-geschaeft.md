+++
fragment = "content"
date = "2022-05-07"
weight = 10
title = "Auf dem Rad ins Open-Source-Geschäft"
background = "white"

summary = "Professioneller, grüner Open-Source-Support auf zwei Rädern – was soll das sein, und was haben IT-Support und Radfahren miteinander zu tun? Willkommen bei velocitux, ich stelle mich kurz vor!"

display_date = true

#[asset]
#image = "justice.jpg"
+++

## Hallo!

Ich bin Open-Source-Enthusiast, und das schon lange. Seit 2003, damals
noch in der Schule, bin ich immer stärker in die Welt der Freien Software
hineingewachsen. Erst als Linux-Nutzer, dann als Administrator in der Schul-IT,
und seit spätestens 2009 als Kontributor und vor allem als Mentor jüngerer
Open-Source-Kontributor\*innen.

Und ich bin Radfahrer, und das aus Überzeugung und sehr aktiv. Ob zur Fortbewegung
oder als Sport, ich mache alles mit dem Fahrrad, viele tausend Kilometer im Jahr.

## Radfahren und IT-Support – Was hat das miteinander zu tun?

Als sich mir im März 2022 die Frage stellte, wie ich meinen Lebensunterhalt
zukünftig bestreiten möchte, suchte ich nach einem Alleinstellungsmerkmal.
In den letzten 10 Jahren hatte ich für verschiedene IT-Unternehmen gearbeitet,
zunächst für die [tarent solutions](https://www.tarent.de/) und dann für
[credativ](https://www.credativ.de/). Beide Unternehmen waren mir am Ende
etwas zu groß und komplex geworden, also war für mich klar, dass ich als
kleiner Dienstleister und insbesondere auch für kleine und mittelständische
Kunden tätig werden möchte. Doch ein wirklicher Aufhänger, der mich von anderen
Anbietern abhebt, fehlte.

So kam mir die Idee, meine beiden Leidenschaften zu verbinden: **Professioneller,
grüner Open-Source-Support auf zwei Rädern**. Nachhaltigkeit ist ein wichtiger
Faktor, insbesondere in der IT. IT-Systeme betreiben unsere Welt, aber verursachen
auch signifikante Emissionen. Und warum soll die Optimierung der IT aus Nachhaltigkeits-
Sicht bei der eingesetzten Hard- und Software aufhören? Alle dienstlichen Reisen
unternehme ich deshalb per Fahrrad (und, sollte für längere Strecken die Zeit nicht
ausreichen, mit der Bahn).

Und nicht zuletzt möchte ich mit dem Konzept auch eine gewisse Einstellung vermitteln:
Auch Berater müssen keine Schlipsträger mit polierten Firmenwagen sein. Professionelle
Beratung und zuverlässiger Support sind keine Gegensätze zu einer lockeren, offenen
und authentischen Umgangsart.

## Wo bleibt mein Steckenpferd, die Freie Software in der Bildung?

Was gehört noch dazu? Natürlich meine persönliche Verankerung im Bildungsbereich.
Während ich mit meiner Firma professionellen Support im Freie-Software-Umfeld
anbiete, bin ich nach wie vor ehrenamtlich für den [Teckids e.V.](https://teckids.org/)
tätig. Auch mit velocitux möchte ich selbstverständlich einen Beitrag zur Bildung
leisten, sowohl durch die Beratung von Bildungseinrichtungen als auch dadurch,
Praktikant\*innen und anderen Einsteiger\*innen Chancen zu eróffnen, die Welt
der Open-Source-IT kennenzulernen.

Alles über die Philosophie und die Ziele von velocitux habe ich
[auf einer dedizierten Seite]({{< ref "/about/philosophy" >}}) zusammengefasst.

## Wir sehen uns!

Nun bin ich gespannt, welche interessanten Projekte sich für mich als nun
selbstständigen Open-Source-Berater ergeben werden. Von Anfang an arbeite ich
bereits mit drei wichtigen Partnern zusammen: dem [Linuxhotel](https://linuxhotel.de/),
wo ich unter anderem als freier Trainer aktiv sein werde, der
[Fre(i)e[n] Software GmbH](https://freiesoftware.gmbh/), mit der ich mich insbesondere
im Bereich Schul-IT zusammengetan habe sowie [Freexian](https://www.freexian.com/), für
die ich unter anderem am LTS- und ELTS-Support für Debian mitwirke.

Ich freue mich auf alle Anfragen rund um Beratung, Schulung und Support rund um
Open-Source-Systeme und -Komponenten!
