+++
fragment = "content"
date = "2022-12-27"
weight = 10
title = "Matrix-Kurse im Linuxhotel"
background = "white"

summary = "Im Juni 2023 finden im Linuxhotel zum ersten Mal zwei von velocitux gestaltete Kurse zu Matrix und Element statt"

display_date = true

[asset]
image = "linuxhotel-matrix.webp"
+++

Der offene Kommunikations-Standard [Matrix](https://matrix.org/) findet
immer mehr Verbreitung, insbesondere zusammen mit seinem
Flagship-Produkt, der Chat- und Kollaborationsplattform
[Element](https://element.io/). Nicht nur Entwickler und
Open-Source-Projekte, sondern auch andere bekannte Plattformen,
Unternehmen, Behörden und Regierungen finden mit Element und Matrix
immer öfter die Möglichkeit, digital souverän und DSGVO-konform
zusammenzuarbeiten. Einen guten Überblick hierüber bietet das
[Matrix Holiday Update](https://matrix.org/blog/2022/12/25/the-matrix-holiday-update-2022),
das traditionell zu Weihnachten das vergangene Jahr zusammenfasst.

## Möglichkeiten für Teams

Mit Element und Matrix haben Teams jeder Art die Möglichkeit, ihre
Kommunikation grundlegend neu zu gestalten, denn die Plattform ist die
einzige, die sowohl vollständig freie Software ist und gleichzeitig die
Anbindung vieler externer Systeme erlaubt. Durch ein durchdachtes und,
für Web-Entwickler, leicht zu verstehendes API sind auch Anpassungen
problemlos möglich. Einige Highlights der Element-Features sind:

-   Strukturierte Spaces mit Sub-Spaces und Chaträumen
-   Normaler Text, Rich Text, Bilder, Videos, Umfragen und mehr im Chat
-   Sprach- und Videoräume
-   Einzel- und Gruppen-Audio-/Video-Konferenzen
-   Erweiterbarkeit durch Widgets mit beliebigen Funktionen in
    Chaträumen

Element ist nicht nur eine Chatsoftware, sondern eine erweiterbare
Plattform, die sich fast unbegrenzt anpassen lässt.

## Kursinhalt im Linuxhotel

Um Teams und Entwickler\*innen den Einstieg zu erleichtern, haben wir in
Zusammenarbeit mit dem [Linuxhotel](https://www.linuxhotel.de/) in Essen
zwei aufeinander aufbauende Schulungen entwickelt (auch einzeln
belegbar).

### Matrix -- Serverbetrieb, Nutzung und Föderation {#matrix--serverbetrieb-nutzung-und-föderation}

Matrix ist das dezentrale Netzwerk, auf dem Element basiert. Als
Protokoll ist es nicht nur eine starke Grundlage für Textchat, Audio-
und Video-Konferenzen und Instant Messaging, sondern bietet
Erweiterbarkeit um fast beliebige Funktionen.

Diese Schulung richtet sich an alle, die die Grundzüge des
Matrix-Protokolls verstehen, die dezentrale Föderation kennenlernen und
den Betrieb eines Servers für sich selber oder das Team inkl. der
gängigsten Kommunikations-Anwendungen erlernen möchten.

-   Was ist ein Homeserver und welche gibt es?
-   Wie kommunizieren Server und Clients?
-   Element Web für Chat, Audio und Video
-   Räume und Spaces verwalten

### Matrix -- Entwicklung und IoT {#matrix--entwicklung-und-iot}

Matrix, der dezentrale Kommunikationsstandard, der insbesondere aus dem
Kommunikationsbereich mit seinem verbreitetsten Client Element bekannt
ist, kann als vielseitige Grundlage für eigene Anwendungen genutzt
werden. Da Matrix im Grunde eine dezentrale, föderierte
Eventual-Consistency-Datenbank ist, sind grundsätzlich alle Anwendungen
denkbar, die sich in JSON-Daten abbilden lassen.

Diese Schulung richtet sich an alle, die sich auf Basis von Matrix mit
der Entwicklung eigener spezieller Clients, z.B. auch im Bereich
Internet of Things, beschäftigen wollen.

-   Matrix-Räume als Datenbank nutzen
-   Grundlagen des Client-to-Server-Protokolls
-   Programmierung mit Python requests
-   (Kurzeinführung) Programmierung mit JavaScript für das Web
-   Anwendung mit MicroPython und ESP-Board

## Kurs-Details und Anmeldung

Die Details zu den Schulungen sowie die Möglichkeit zur Anmeldung sind
auf den jeweiligen Kurs-Seiten des Linuxhotels zu finden:

-   [Matrix -- Serverbetrieb, Nutzung und
    Föderation](https://www.linuxhotel.de/course/matrix-de)
-   [Matrix -- Entwicklung und
    IoT](https://www.linuxhotel.de/course/matrix-dev-de)

Das Linuxhotel ist, nebden einem der "ungewöhnlichsten Hotels im
Ruhrgebiet", ein Seminar-Zentrum für eine breite Themen-Auswahl aus dem
Open-Source-Bereich. Die Trainer\*innen sind ausgewiesene Expert\*innen,
die meistens intensiv an den Systemen, die sie schulen, als
Entwickler\*innen beteiligt sind. velocitux ist Partner des Linuxhotels
für verschiedene Kursthemen, darunter die Kurze
[Linux-Grundlagen](https://www.linuxhotel.de/course/linux_grundlagen-de),
[Linux-Administration](https://www.linuxhotel.de/course/admin_grundlagen-de)
und
[PostgreSQL-Datenbank](https://www.linuxhotel.de/course/postgresql-de).
