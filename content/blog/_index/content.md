+++
fragment = "content"
weight = 10

title = "Blog und News"
background = "white"
+++

Was gibt es Neues in der Welt der Open-Source-IT?
Welche Themen werden auf Konferenzen diskutiert?
Was haben wir als Firma, unsere Mitarbeitenden, Partnerinnen und Partner gelernt oder erarbeitet?

Unsere Arbeit lebt vom Austausch und Kompetenz braucht
Wissen und Gespräche über Erfahrungen, Projekte und Bewegungen.
Deshalb wollen wir aktiv berichten und freuen uns über Rückmeldungen
und Fragen zu unseren Posts.
