+++
fragment = "content"
weight = 10
date = "2022-04-03"

background = "light"
title = "Zusammenarbeit statt Bürokratie"
+++

Bei velocitux kommt es uns darauf an, hochwertigen Support und Beratung
zu bieten und dabei Freude an freier Software und technischen Herausforderungen
zu haben.

Als kleines Unternehmen setzen wir dabei auf ein Netzwerk unterschiedlicher
Menschen, die entweder direkt bei uns arbeiten oder als freie Partnerinnen
oder Partner mit uns vernetzt sind.
