+++
title = "Jonathan Weth"
weight = 20
date = "2022-11-01"

position = "Full-Stack-Entwickler (Extern)"
#company = ""

lives_in = "Hamburg"

[[icons]]
  icon = "fas fa-envelope"
  url = "mailto:contact@jonathanweth.de"

[asset]
image = "jonathan.webp"
+++

Jonathan ist bereits seit seiner Jugend aktiver Entwickler und
FOSS-Kontributor sowie seit mehreren Jahren Co-Maintainer des
Softwareprojekts [AlekSIS](https://aleksis.org/de/). Dadurch
hat er neben dem Bereich Freie Software auch Erfahrungen im
Qualitäts- und Anforderungsmanagement. Aktuell macht er einen
Bundesfreiwilligendienst beim Technischen Hilfswerk. Als freier
Mitarbeiter unterstützt er bei velocitux vor allem die Anpassung
von Software für und die Beratung von Bildungseinrichtungen.
