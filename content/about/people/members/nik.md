+++
title = "Dominik George"
weight = 10
date = "2022-04-03"

position = "Geschäftsführender Gesellschafter"
company = "velocitux UG"

lives_in = "Bonn / Wermelskirchen"

[[icons]]
  icon = "fas fa-envelope"
  url = "mailto:nik@velocitux.com"

[asset]
image = "nik.jpg"
+++

Dominik George, kurz Nik, wurde 1990 geboren und sammelt seit
dem Teenager-Alter Erfahrungen in der Systemadministration und in
der Kontribution zu freier Software. Insbesondere hat er über die
Jahre hinweg in mehreren großen Communities, allen voran dem
[Debian-Projekt](https://debian.org), mitgewirkt, meistens
mit einem starken Fokus auf Bildung und die Arbeit mit Schulen,
Kindern und Jugendlichen, z.B. als Gründer und Leiter beim
[Teckids e.V.](https://teckids.org), als Co-Maintainer und Mentor
im Softwareprojekt [AlekSIS®](https://aleksis.org) oder als
Trainer im [Linuxhotel](https://linuxhotel.de).
