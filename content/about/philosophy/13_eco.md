+++
fragment = "item"
weight = "13"
align = "right"

background = "light"

title = "Ökologie und Nachhaltigkeit"

[asset]
icon = "fas fa-leaf"
+++

Digitalisierung und Technisierung verbraucht immer mehr Ressourcen,
insbesondere elektrische Energie. Doch sowohl mit der richtigen Auswahl
technischer Komponenten selber als auch mit etwas Kreativität drumherum
können wir dazu beitragen, Auswirkungen auf Umwelt und Klima zu minimieren
und zu kompensieren.

* Hardware und Dienstleistungen kaufen wir vorrangig von Anbietern ein,
  die selber auf ökologische Nachhaltigkeit achten
* Freie Software ist häufig dank der fehlenden "geplanten Obsoleszenz"
  nachhaltiger
* Wir arbeiten remote oder reisen grün, per Fahrrad, Bahn und Bus

Wir sind professionelle Berater, aber trotzdem Fahrrad-Fans und wollen,
dass unsere Dienstleistungen so nachhaltig wie möglich bleiben!
