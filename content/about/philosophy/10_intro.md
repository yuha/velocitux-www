+++
fragment = "content"
weight = "10"
background = "white"

title = "Unsere Ziele und Philosophie"
+++

velocitux ist mehr als nur ein IT-Dienstleister. Mit unseren
Leistungen und Fähigkeiten wollen wir zu einer bessere, digitalen
sowie realen, Welt beitragen.

Unseren Beitrag leisten wir direkt mit unserer Arbeit, denn wir
unterstützen am Liebsten Kunden, die auch für gute Zwecke arbeiten.
Aber auch darüberhinaus setzen wir auf vier Felder, durch die wir
uns professionell und sozial abheben möchten.
