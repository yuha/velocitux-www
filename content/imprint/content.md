+++
fragment = "content"
date = "2022-04-07"
weight = 100
title = "Impressum"
background = "white"
+++

## Angaben gemäß § 5 TMG

velocitux UG (haftungsbeschränkt)
Hilfringhauser Str. 84  
42929 Wermelskirchen

Handelsregister: 54321  
Registergericht: Wermelskirchen  

Vertreten durch: Dominik George

## Kontakt

Telefon: [+49 2196 7389727](tel:+4921967389727)  
E-Mail: [info@velocitux.com](mailto:info@velocitux.com)

## Redaktionell verantwortlich

Dominik George

## EU-Streitschlichtung

Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit:
[https://ec.europa.eu/consumers/odr/](https://ec.europa.eu/consumers/odr/)

## Verbraucherstreitbeilegung/Universalschlichtungsstelle

Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer
Verbraucherschlichtungsstelle teilzunehmen.
