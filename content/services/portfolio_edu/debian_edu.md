+++
weight = 11
title = "Debian Edu / Skolelinux"

[asset]
image = "debian_edu.png"
url = "https://blends.debian.org/edu/"
+++

Debian Edu, auch bekannt als Skolelinux, ist eine auf die Anforderungen
von Schulen ausgerichtete Variante der Linux-Distribution
[Debian](https://debian.org). Sie eignet sich zum Betrieb von Schul-Servern,
Arbeitsplatzrechnern in Rechnerräumen sowie Laptops und Tablets.

Wir beraten und unterstützen im Aufbau von auf Debian Edu basierenden
Schulnetz-Infrastrukturen sowie bei der Integration mit anderen Komponenten.
