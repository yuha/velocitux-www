+++
fragment = "item"
weight = "14"
align = "left"

background = "secondary"

title = "Fortbildung und Schulung"

[asset]
icon = "fas fa-graduation-cap"
+++

Technologie und IT sollten keine Geheimnisse sein, sondern
für jeden mit entsprechendem Interesse verständlich und
nachvollziehbar sein. Wir sowie unsere Partnerinnen und
Partner vermitteln jederzeit gerne Wissen und geben unsere
Erfahrungen weiter.

* Workshops zu aufgebauten IT-Umgebungen
* Schulungen zu Experten-Themen, z.B. aus den Bereichen
  **Linux**, **Debian**, **Netzwerk**, **Python-Entwicklung**
  uvm.
* Zusammenarbeit mit dem einmaligen Schulungszentrum
  [Linuxhotel](https://linuxhotel.de)
