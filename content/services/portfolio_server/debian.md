+++
weight = 10
title = "Debian"

[asset]
image = "debian.png"
url = "https://debian.org"
+++

Die GNU/Linux-Distribution Debian ist eines der robustesten und verbreitetsten
Betriebssysteme für Server und Arbeitsplatzrechner.

Wir bieten Beratung und Schulung, Unterstützung beim Betrieb bis hin zu
Server-Management sowie die Erstellung und Pflege individueller Pakete, bis
hin zur Pflege dieser Pakete in den Repositories von Debian selber.
