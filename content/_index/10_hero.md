+++
fragment = "hero"
weight = "10"

background = "light"
asset = { image = "artwork/logo/velocitux-logo.svg" }
subtitle = "Professioneller, grüner Open-Source-Support auf zwei Rädern"
+++
