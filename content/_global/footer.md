+++
fragment = "footer"
date = "2022-04-07"
weight = 1200
background = "light"

menu_title = "Partner und Links"
+++

#### Kontakt

velocitux UG (haftungsbeschränkt)  
Hilfringhauser Str. 84  
42929 Wermelskirchen

E-Mail: [info@velocitux.com](mailto:info@velocitux.com)
