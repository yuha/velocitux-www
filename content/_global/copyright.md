+++
fragment = "copyright"
date = "2022-04-07"
weight = 1250
background = "dark"

attribution = true
copyright = "© 2022 Dominik George, velocitux UG"
+++
