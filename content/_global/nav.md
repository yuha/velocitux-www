+++
fragment = "nav"
date = "2022-04-07"
weight = 0
background = "dark"
search = false
sticky = true

[breadcrumb]
  display = false
  background = "light"

[repo_button]
  text = "Kontakt aufnehmen"
  icon = "fas fa-paper-plane"
  url = "/contact"

# Branding options
[asset]
  image = "artwork/logo/velocitux-logo.svg"
  text = "velocitux"
+++
